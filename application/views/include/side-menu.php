<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Ruler - Admin Server</title>

    <!-- Bootstrap Core CSS -->
    <link href="<?= base_url ('vendor/bootstrap/css/bootstrap.min.css') ?>" rel="stylesheet">

    <!-- MetisMenu CSS -->
    <link href="<?= base_url ('vendor/metisMenu/metisMenu.min.css') ?>" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="<?= base_url ('dist/css/sb-admin-2.css') ?>" rel="stylesheet">

    <!-- Morris Charts CSS -->
    <link href="<?= base_url ('vendor/morrisjs/morris.css') ?>" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="<?= base_url ('vendor/font-awesome/css/font-awesome.min.css') ?>" rel="stylesheet" type="text/css">

    <style>
        .user-row:first-child { display: none; }
    </style>

</head>

<body>

    <div id="wrapper">

        <!-- Navigation -->
        <nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="index.html">Ruler</a>
            </div>
            <!-- /.navbar-header -->

            <ul class="nav navbar-top-links navbar-right">
                <!-- /.dropdown -->
                <li class="dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                        <i class="fa fa-user fa-fw"></i> <i class="fa fa-caret-down"></i>
                    </a>
                    <ul class="dropdown-menu dropdown-user">
                        <li class="divider"></li>
                        <li><a href="<?= base_url('home/sair') ?>"><i class="fa fa-sign-out fa-fw"></i> Sair</a>
                        </li>
                    </ul>
                    <!-- /.dropdown-user -->
                </li>
                <!-- /.dropdown -->
            </ul>
            <!-- /.navbar-top-links -->

            <div class="navbar-default sidebar" role="navigation">
                <div class="sidebar-nav navbar-collapse">
                    <ul class="nav" id="side-menu">
                        <li class="sidebar-search">
                            <div class="input-group custom-search-form">
                                <input type="text" class="form-control" placeholder="Search...">
                                <span class="input-group-btn">
                                <button class="btn btn-default" type="button">
                                    <i class="fa fa-search"></i>
                                </button>
                            </span>
                            </div>
                            <!-- /input-group -->
                        </li>
                        <li>
                            <a href="<?= base_url('dashboard') ?>"><i class="fa fa-dashboard fa-fw"></i> Dashboard</a>
                        </li>
                        <li>
                            <a href="<?= base_url('servidores') ?>"><i class="fa fa-edit fa-fw"></i> Servidores</a>
                        </li>
                        <li>
                            <a href="<?= base_url('usuarios') ?>"><i class="fa fa-edit fa-fw"></i> Usuários</a>
                        </li>
                        <li>
                            <a href="<?= base_url('credenciais') ?>"><i class="fa fa-edit fa-fw"></i> Credenciais</a>
                        </li>
                        <li>
                            <a href="<?= base_url('equipamentos') ?>"><i class="fa fa-edit fa-fw"></i> Equipamentos </a>
                        </li>
                        <li>
                            <a href="<?= base_url('fornecedores') ?>"><i class="fa fa-edit fa-fw"></i> Fornecedores</a>
                        </li>
                        <li>
                            <a href="<?= base_url('garantia') ?>"><i class="fa fa-edit fa-fw"></i> Controle garantia</a>
                        </li>
                        <li>
                            <a href="<?= base_url('wiki') ?>"><i class="fa fa-edit fa-fw"></i> Wiki</a>
                        </li>
                        <li>
                            <a href="#"><i class="fa fa-files-o fa-fw"></i> Relatórios<span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                                <li>
                                    <a href="blank.html">Credenciais</a>
                                </li>
                                <li>
                                    <a href="login.html">Equipamentos</a>
                                </li>
                                <li>
                                    <a href="login.html">Servidores</a>
                                </li>
                                <li>
                                    <a href="login.html">Usuários</a>
                                </li>
                            </ul>
                            <!-- /.nav-second-level -->
                        </li>
                    </ul>
                </div>
                <!-- /.sidebar-collapse -->
            </div>
            <!-- /.navbar-static-side -->
        </nav>

        </div>
    <!-- /#wrapper -->

    <!-- jQuery -->
    <script src="<?= base_url('vendor/jquery/jquery.min.js') ?>"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="<?= base_url('vendor/bootstrap/js/bootstrap.min.js') ?>"></script>

    <!-- Metis Menu Plugin JavaScript -->
    <script src="<?= base_url('vendor/metisMenu/metisMenu.min.js')?>"></script>

    <!-- Morris Charts JavaScript -->
    <script src="<?= base_url('vendor/raphael/raphael.min.js') ?>"></script>
    <script src="<?= base_url('vendor/morrisjs/morris.min.js') ?>"></script>
    <script src="<?= base_url('data/morris-data.js') ?>"></script>

    <!-- Custom Theme JavaScript -->
    <script src=" <?= base_url('/dist/js/sb-admin-2.js') ?>"></script>

