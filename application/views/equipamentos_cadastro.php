<div id="page-wrapper">

  <div class="row">
    <div class="col-lg-12">
      <h1 class="page-header">Cadastro de Equipamentos</h1>
    </div>
  </div>

  <div class="row">
    <div class="col-lg-12">
      <div class="panel panel-default">
        <div class="panel-heading">
          Formulário de Cadastro de Equipamentos
        </div>
        <div class="panel-body">
          <div class="row">
            <div class="col-lg-12">
              <form role="form" method="post" action="<?= base_url('equipamentos/grava_cadastro') ?>" enctype="multipart/form-data">
                <div class="form-group">
                  <label>Nome:</label>
                  <input class="form-control" name="nome" placeholder="Digite o Nome do Equipamento">
                </div>
                <div class="form-group">
                  <label>Descrição:</label>
                  <input class="form-control" name="descricao" placeholder="Digite a Descrição do Equipamento">
                </div>
                <div class="form-group">
                  <label>Marca:</label>
                  <input class="form-control" name="marca" placeholder="Digite a Marca do Equipamento">
                </div>
                <div class="form-group">
                  <label>Modelo:</label>
                  <input class="form-control" name="modelo" placeholder="Digite o Modelo do Equipamento">
                </div>
                <div class="form-group">
                  <label>Número de Série:</label>
                  <input class="form-control" name="numeroSerie" placeholder="Digite o Número de Série do Equipamento">
                </div>
                <div class="form-group">
                  <label>Data da Compra:</label>
                  <input class="form-control" name="dataCompra" placeholder="Digite a Data da Compra do Equipamento">
                </div>
                <div class="form-group">
                  <label>Tipo de Garantia:</label>
                  <input class="form-control" name="tipoGarantia" placeholder="Digite o Tipo de Garantia do Equipamento">
                </div>
                <div class="form-group">
                  <label>Tempo de Garantia:</label>
                  <input class="form-control" name="tempoGarantia" placeholder="Digite o Tempo de Garantia do Equipamento">
                </div>
                <div class="form-group">
                  <label>Nota Fiscal:</label>
                  <input class="form-control" name="notaFiscal" placeholder="Nota Fiscal">
                </div>
                <div class="form-group">
                   <label for="fornecedores">Fornecedor</label>
                       <select class="form-control" name="idFornecedor">
                          <option value=""> Selecione... </option>
                          <?php foreach ($fornecedores as $fo) { ?>
                          <option value="<?= $fo->id ?>"> <?= $fo->razaoSocial?> </option>            
                          <?php } ?>
                       </select>                       
                </div>
                <div class="form-group">
                  <label>Acesso:</label>
                  <input class="form-control" name="acesso" placeholder="Digite o Hostname ou IP do Equipamento">
                </div>
                <div class="form-group">
                  <label>Usuário:</label>
                  <input class="form-control" name="usuario" placeholder="Digite o Usuario de Aceso ao Equipamento">
                </div>
                <div class="form-group">
                  <label>Senha:</label>
                  <input class="form-control" name="senha" placeholder="Digite a Senha de Acesso ao Equipamento">
                </div>

                <button type="submit" class="btn btn-default">Cadastrar</button>
                <button type="reset" class="btn btn-default">Limpar</button>
              </form>
            </div>
            <!-- /.col-lg-6 (nested) -->
          </div>
          <!-- /.row (nested) -->
        </div>
        <!-- /.panel-body -->
      </div>
      <!-- /.panel -->
    </div>
    <!-- /.col-lg-12 -->
  </div>

  </body>

  </html>