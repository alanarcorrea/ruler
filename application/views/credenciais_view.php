<div id="page-wrapper">
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">Credenciais</h1>
                    </div>
                </div>
                <?php
                    // se houver uma variável de sessão definida irá exibir a mensagem
                    if ($this->session->has_userdata('mensa')) {
                    // obtém os valores atribuídos às variáveis de sessão
                    $mensa = $this->session->flashdata('mensa');
                    $tipo = $this->session->flashdata('tipo');
                
                    // if ($tipo==1)
                        if ($tipo) {
                            echo "<div class='alert alert-success'>";
                            echo "<strong>Successo!! </strong>" . $mensa; 
                            echo "</div>";
                        } else {
                            echo "<div class='alert alert-danger'>";
                            echo "<strong>Erro... </strong>" . $mensa; 
                            echo "</div>";
                        }                
                    }            
                ?>

    <!--Tabela Módulos -->
    <div class="col-sm-2 navbar-right">
        <a href="<?= base_url('clientes/incluir') ?>" class="btn btn-success btn-sm">
                        <span class="glyphicon glyphicon-new-window"></span> Nova Credencial</a>
    </div>
    <br> <br> 
               <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            Credenciais
                        </div>                       
                        <div class="panel-body">
                            <div class="table-responsive">
                                <table class="table table-striped table-bordered table-hover">
                                    <thead>
                                        <tr>
                                            <th>Host</th>
                                            <th>Acesso</th>
                                            <th>Usuário</th>
                                            <th>Senha</th>
                                            <th>Descrição</th>
                                            <th>Ações</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php foreach ($credenciais as $cred) { ?>
                                            <tr>
                                                <td><?= $cred->host ?></td>
                                                <td><?= $cred->acesso ?></td>
                                                <td><?= $cred->usuario ?></td>
                                                <td><?= $cred->senha ?></td>
                                                <td><?= $cred->descricao ?></td>
                                                <td>
                                                    <a href="<?= base_url().'credenciais/alterar/'.$cred->id ?>">
                                                        <span class="glyphicon glyphicon-pencil" title="Alterar"></span></a> &nbsp;&nbsp; 
                                                    <a href="<?= base_url().'credenciais/destacar/'.$cred->id ?>">
                                                        <span class="glyphicon glyphicon-heart" title="Destacar"></span></a> &nbsp;&nbsp;
                                                    <a href="<?= base_url().'credenciais/desativar/'.$cred->id ?>">
                                                        <span class="glyphicon glyphicon-ban-circle" title="Desativar"></span></a> &nbsp;&nbsp;
                                                    <a href="<?= base_url().'credenciais/excluir/modulos'.$cred->id ?>"
                                                        onclick="return confirm('Confirma Exclusão da Credencial\'<?= $cred->host ?>\'?')">
                                                        <span class="glyphicon glyphicon-remove" title="Excluir"></span></a>&nbsp;&nbsp;
                                            </td>
                                        </tr>
                                        <?php } ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
    
    </body>
</html>