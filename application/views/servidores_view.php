<div id="page-wrapper">
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">Servidores</h1>
                    </div>
                </div>
                <?php
                    // se houver uma variável de sessão definida irá exibir a mensagem
                    if ($this->session->has_userdata('mensa')) {
                    // obtém os valores atribuídos às variáveis de sessão
                    $mensa = $this->session->flashdata('mensa');
                    $tipo = $this->session->flashdata('tipo');
                
                    // if ($tipo==1)
                        if ($tipo) {
                            echo "<div class='alert alert-success'>";
                            echo "<strong>Successo!! </strong>" . $mensa; 
                            echo "</div>";
                        } else {
                            echo "<div class='alert alert-danger'>";
                            echo "<strong>Erro... </strong>" . $mensa; 
                            echo "</div>";
                        }                
                    }            
                ?>

    <!--Tabela Módulos -->
    <div class="col-sm-2 navbar-right">
        <a href="<?= base_url('servidores/cadastrar') ?>" class="btn btn-success btn-sm">
                        <span class="glyphicon glyphicon-new-window"></span> Novo Servidor</a>
    </div>
    <br> <br> 
               <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            Servidores
                        </div>                       
                        <div class="panel-body">
                            <div class="table-responsive">
                                <table class="table table-striped table-bordered table-hover">
                                    <thead>
                                        <tr>
                                            <th>Hostname</th>
                                            <th>IP</th>
                                            <th>Sistema Operacional</th>
                                            <th>Funções</th>
                                            <th>Domínio</th>
                                            <th>Ações</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php foreach ($servidor as $srv) { ?>
                                            <tr>
                                                <td><?= $srv->hostname ?></td>
                                                <td><?= $srv->ip ?></td>
                                                <td><?= $srv->sistemaOperacional ?></td>
                                                <td>funções</td>
                                                <td><?= $srv->dominio?></td>
                                                <td>
                                                    <a href="<?= base_url().'servidores/alterar/'.$srv->id ?>">
                                                        <span class="glyphicon glyphicon-pencil" title="Alterar"></span></a> &nbsp;&nbsp; 
                                                    <a href="<?= base_url().'servidores/visualizar/'.$srv->id ?>">
                                                        <span class="glyphicon glyphicon-search" title="Detalhes"></span></a> &nbsp;&nbsp;
                                                    <a href="<?= base_url().'servidores/excluir/'.$srv->id ?>"
                                                        onclick="return confirm('Confirma Exclusão do Servidor\'<?= $srv->hostname ?>\'?')">
                                                        <span class="glyphicon glyphicon-remove" title="Excluir"></span></a>&nbsp;&nbsp;
                                            </td>
                                        </tr>
                                        <?php } ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
    
    </body>
</html>