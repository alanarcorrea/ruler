<div id="page-wrapper">

  <div class="row">
    <div class="col-lg-12">
      <h1 class="page-header">Cadastro de Fornecedores</h1>
    </div>
  </div>

  <div class="row">
    <div class="col-lg-12">
      <div class="panel panel-default">
        <div class="panel-heading">
          Formulário de Cadastro de Fornecedores
        </div>
        <div class="panel-body">
          <div class="row">
            <div class="col-lg-12">
              <form role="form" method="post" action="<?= base_url('fornecedores/grava_cadastro') ?>" enctype="multipart/form-data">
                <div class="form-group">
                  <label>Razão Social:</label>
                  <input class="form-control" name="razaoSocial" placeholder="Digite a Razão Social do Fornecedor">
                </div>
                <div class="form-group">
                  <label>E-mail:</label>
                  <input class="form-control" name="email" placeholder="Digite o Email do Fornecedor">
                </div>
                <div class="form-group">
                  <label>Telefone:</label>
                  <input class="form-control" name="telefone" placeholder="Digite o Telefone do Fornecedor">
                </div>
                <div class="form-group">
                  <label>CNPJ:</label>
                  <input class="form-control" name="cnpj" placeholder="Digite o CNPJ do Fornecedor">
                </div>
                <div class="form-group">
                  <label>CEP:</label>
                  <input class="form-control" name="cep" placeholder="Digite o CEP do Fornecedor">
                </div>
                <div class="form-group">
                  <label>Endereço:</label>
                  <input class="form-control" name="endereco" placeholder="Digite o Endereco do Fornecedor">
                </div>
                <div class="form-group">
                  <label>Bairro:</label>
                  <input class="form-control" name="bairro" placeholder="Digite o Bairro do Fornecedor">
                </div>
                <div class="form-group">
                  <label>Cidade:</label>
                  <input class="form-control" name="cidade" placeholder="Digite a Cidade do Fornecedor">
                </div>
                <div class="form-group">
                  <label>Estado:</label>
                  <input class="form-control" name="estado" placeholder="Digite o Estado do Fornecedor">
                </div>
                 <div class="form-group">
                  <label>Site:</label>
                  <input class="form-control" name="site" placeholder="Digite o Site do Fornecedor">
                </div>
                <button type="submit" class="btn btn-default">Cadastrar</button>
                <button type="reset" class="btn btn-default">Limpar</button>
              </form>
            </div>
            <!-- /.col-lg-6 (nested) -->
          </div>
          <!-- /.row (nested) -->
        </div>
        <!-- /.panel-body -->
      </div>
      <!-- /.panel -->
    </div>
    <!-- /.col-lg-12 -->
  </div>

  </body>

  </html>