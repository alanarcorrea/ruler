<div id="page-wrapper">
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">Equipamentos</h1>
                    </div>
                </div>
                <?php
                    // se houver uma variável de sessão definida irá exibir a mensagem
                    if ($this->session->has_userdata('mensa')) {
                    // obtém os valores atribuídos às variáveis de sessão
                    $mensa = $this->session->flashdata('mensa');
                    $tipo = $this->session->flashdata('tipo');
                
                    // if ($tipo==1)
                        if ($tipo) {
                            echo "<div class='alert alert-success'>";
                            echo "<strong>Successo!! </strong>" . $mensa; 
                            echo "</div>";
                        } else {
                            echo "<div class='alert alert-danger'>";
                            echo "<strong>Erro... </strong>" . $mensa; 
                            echo "</div>";
                        }                
                    }            
                ?>

    <!--Tabela Módulos -->
    <div class="col-sm-2 navbar-right">
        <a href="<?= base_url('equipamentos/cadastrar') ?>" class="btn btn-success btn-sm">
                        <span class="glyphicon glyphicon-new-window"></span> Novo Equipamento</a>
    </div>
    <br> <br> 
               <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            Equipamentos
                        </div>                       
                        <div class="panel-body">
                            <div class="table-responsive">
                                <table class="table table-striped table-bordered table-hover">
                                    <thead>
                                        <tr>
                                            <th>Nome</th>
                                            <th>Marca</th>
                                            <th>Modelo</th>
                                            <th>Número Série</th>
                                            <th>Data Compra</th>
                                            <th>Ações</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php foreach ($equipamentos as $equip) { ?>
                                            <tr>
                                                <td><?= $equip->nome ?></td>
                                                <td><?= $equip->marca ?></td>
                                                <td><?= $equip->modelo?></td>
                                                <td><?= $equip->numeroSerie ?></td>
                                                <td><?= date_format(date_create ($equip->dataCompra),'d/m/Y') ?></td>
                                                <td>
                                                    <a href="<?= base_url().'equipamentos/alterar/'?>">
                                                        <span class="glyphicon glyphicon-pencil" title="Alterar"></span></a> &nbsp;
                                                    <a href="<?= base_url().'equipamentos/excluir/modulos'?>"
                                                        onclick="return confirm('Confirma Exclusão do Equipamento)">
                                                        <span class="glyphicon glyphicon-remove" title="Excluir"></span></a>&nbsp;&nbsp;
                                            </td>
                                        </tr>
                                        <?php } ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
    
    </body>
</html>