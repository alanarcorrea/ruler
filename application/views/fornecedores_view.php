<div id="page-wrapper">
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">Fornecedores</h1>
                    </div>
                </div>
                <?php
                    // se houver uma variável de sessão definida irá exibir a mensagem
                    if ($this->session->has_userdata('mensa')) {
                    // obtém os valores atribuídos às variáveis de sessão
                    $mensa = $this->session->flashdata('mensa');
                    $tipo = $this->session->flashdata('tipo');
                
                    // if ($tipo==1)
                        if ($tipo) {
                            echo "<div class='alert alert-success'>";
                            echo "<strong>Successo!! </strong>" . $mensa; 
                            echo "</div>";
                        } else {
                            echo "<div class='alert alert-danger'>";
                            echo "<strong>Erro... </strong>" . $mensa; 
                            echo "</div>";
                        }                
                    }            
                ?>

    <!--Tabela Módulos -->
    <div class="col-sm-2 navbar-right">
        <a href="<?= base_url('fornecedores/cadastrar') ?>" class="btn btn-success btn-sm">
                        <span class="glyphicon glyphicon-new-window"></span> Novo Fornecedor</a>
    </div>
    <br> <br> 
               <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            Lista de Fornecedores
                        </div>                       
                        <div class="panel-body">
                            <div class="table-responsive">
                                <table class="table table-striped table-bordered table-hover">
                                    <thead>
                                        <tr>
                                            <th>Razão Social</th>
                                            <th>E-mail</th>
                                            <th>Telefone</th>
                                            <th>Endereço</th>
                                            <th>Cidade/Estado</th>
                                            <th>Ações</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php foreach ($fornecedores as $for) { ?>
                                            <tr>
                                                <td><?= $for->razaoSocial ?></td>
                                                <td><?= $for->email ?></td>
                                                <td><?= $for->telefone ?></td>
                                                <td><?= $for->endereco ?></td>
                                                <td><?= $for->cidade ?> / <?= $for->estado ?></td>
                                                <td>
                                                    <a href="<?= base_url().'fornecedores/alterar/'.$for->id ?>">
                                                        <span class="glyphicon glyphicon-pencil" title="Alterar"></span></a> &nbsp;&nbsp;
                                                    <a href="<?= base_url().'fornecedores/visualizar/'.$for->id ?>">
                                                        <span class="glyphicon glyphicon-search" title="Detalhes"></span></a> &nbsp;&nbsp;
                                                    <a href="<?= base_url().'fornecedores/excluir/'.$for->id ?>"
                                                        onclick="return confirm('Confirma Exclusão do Fornecedor\'<?= $for->razaoSocial ?>\'?')">
                                                        <span class="glyphicon glyphicon-remove" title="Excluir"></span></a>&nbsp;&nbsp;
                                            </td>
                                        </tr>
                                        <?php } ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
    
    </body>
</html>