<div id="page-wrapper">

  <div class="row">
    <div class="col-lg-12">
      <h1 class="page-header">Alteração de Fornecedores</h1>
    </div>
  </div>

  <div class="row">
    <div class="col-lg-12">
      <div class="panel panel-default">
        <div class="panel-heading">
          Formulário de Alteração de Fornecedores
        </div>
        <div class="panel-body">
          <div class="row">
            <div class="col-lg-12">
              <form role="form" method="post" action="<?= base_url('fornecedores/grava_alteracao') ?>" enctype="multipart/form-data">
              <input type="hidden" name="id" value="<?= $fornecedor->id ?>">

                <div class="form-group">
                  <label>Razão Social:</label>
                  <input class="form-control" name="razaoSocial" id="razaoSocial" value="<?= $fornecedor->razaoSocial ?>">
                </div>
                <div class="form-group">
                  <label>E-mail:</label>
                  <input class="form-control" name="email" id="email" value="<?= $fornecedor->email ?>">
                </div>
                <div class="form-group">
                  <label>Telefone:</label>
                  <input class="form-control" name="telefone" id="telefone" value="<?= $fornecedor->telefone ?>">
                </div>
                <div class="form-group">
                  <label>CNPJ:</label>
                  <input class="form-control" name="cnpj" id="cnpj" value="<?= $fornecedor->cnpj ?>">
                </div>
                <div class="form-group">
                  <label>CEP:</label>
                  <input class="form-control" name="cep" id="cep" value="<?= $fornecedor->cep ?>">
                </div>
                <div class="form-group">
                  <label>Endereço:</label>
                  <input class="form-control" name="endereco" id="endereco" value="<?= $fornecedor->endereco ?>" >
                </div>
                <div class="form-group">
                  <label>Bairro:</label>
                  <input class="form-control" name="bairro"id="bairro" value="<?= $fornecedor->bairro ?>" >
                </div>
                <div class="form-group">
                  <label>Cidade:</label>
                  <input class="form-control" name="cidade" id="cidade" value="<?= $fornecedor->cidade ?>">
                </div>
                <div class="form-group">
                  <label>Estado:</label>
                  <input class="form-control" name="estado" id="estado" value="<?= $fornecedor->estado ?>">
                </div>
                <div class="form-group">
                  <label>Site:</label>
                  <input class="form-control" name="site" id="site" value="<?= $fornecedor->site ?>">
                </div>
                <button type="submit" class="btn btn-default">Cadastrar</button>
                <button type="reset" class="btn btn-default">Limpar</button>
              </form>
            </div>
            <!-- /.col-lg-6 (nested) -->
          </div>
          <!-- /.row (nested) -->
        </div>
        <!-- /.panel-body -->
      </div>
      <!-- /.panel -->
    </div>
    <!-- /.col-lg-12 -->
  </div>

  </body>

  </html>