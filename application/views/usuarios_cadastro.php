<div id="page-wrapper">

    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">Cadastro de Usuários</h1>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Formulário de Cadastro de Usuários
                </div>
                <div class="panel-body">
                    <div class="row">
                        <div class="col-lg-12">
                            <form role="form" method="post" action="<?= base_url('usuarios/grava_cadastro') ?>" enctype="multipart/form-data">
                                <div class="form-group">
                                    <label>Nome Exibição:</label>
                                    <input class="form-control" name="name" placeholder="Digite o nome de exibição do usuário">
                                </div>
                                <div class="form-group">
                                    <label>Nome</label>
                                    <input class="form-control" name="givenname" placeholder="Digite o nome do usuário">
                                </div>
                                <div class="form-group">
                                    <label>Login Usuário</label>
                                    <input class="form-control" name="userprincipalname" placeholder="Digite o login do usuário">
                                </div>
                                <div class="form-group">
                                    <label>Sobrenome</label>
                                    <input class="form-control" name="sn" placeholder="Digite o sobrenome do usuário">
                                </div>  
                                <div class="form-group">
                                    <label>Senha</label>
                                    <input class="form-control" name="password" placeholder="Digite a senha do usuário">
                                </div>           
                                <button type="submit" name="upload" class="btn btn-default">Cadastrar</button>
                                <button type="reset" class="btn btn-default">Limpar</button>
                            </form>
                        </div>
                        <!-- /.col-lg-6 (nested) -->
                    </div>
                    <!-- /.row (nested) -->
                </div>
                <!-- /.panel-body -->
            </div>
            <!-- /.panel -->
        </div>
        <!-- /.col-lg-12 -->
    </div>

</body>
</html>