<div id="page-wrapper">

    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">Cadastro de Servidores</h1>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Formulário de Cadastro de Servidor
                </div>
                <div class="panel-body">
                    <div class="row">
                        <div class="col-lg-12">
                            <form role="form" method="post" action="<?= base_url('servidores/grava_cadastro') ?>" enctype="multipart/form-data">
                                <div class="form-group">
                                    <label>Hostname</label>
                                    <input class="form-control" name="hostname" placeholder="Digite o hostname do servidor">

                                </div>
                                <div class="form-group">
                                    <label>IP</label>
                                    <input class="form-control" name="ip" placeholder="Digite o IP do servidor">
                                </div>
                                <div class="form-group">
                                    <label>Domínio</label>
                                    <input class="form-control" name="dominio" placeholder="Digite o domínio do servidor">
                                </div>
                                <div class="form-group">
                                    <label>DC</label>
                                    <input class="form-control" name="dc" placeholder="Digite a dc do servidor">
                                </div>               
                                <div class="form-group">
                                    <label for="sistemaOperacional">Sistema Operacional</label>
                                    <select class="form-control" name="idSistemaOperacional">
                                        <option value=""> Selecione... </option>
                                            <?php foreach ($sistemaOperacional as $so) { ?>
                                        <option value="<?= $so->id ?>"> <?= $so->nome?> </option>            
                                            <?php } ?>
                                    </select>                       
                                </div>
                                <div class="form-group">
                                    <label>Usuário</label>
                                    <input class="form-control" name="usuario" placeholder="Digite o usuário de acesso ao servidor">
                                </div>  
                                <div class="form-group">
                                    <label>Senha</label>
                                    <input class="form-control" name="senha" placeholder="Digite a senha de acesso ao servidor">
                                </div>            
                                <button type="submit" name="upload" class="btn btn-default">Cadastrar</button>
                                <button type="reset" class="btn btn-default">Limpar</button>
                            </form>
                        </div>
                        <!-- /.col-lg-6 (nested) -->
                    </div>
                    <!-- /.row (nested) -->
                </div>
                <!-- /.panel-body -->
            </div>
            <!-- /.panel -->
        </div>
        <!-- /.col-lg-12 -->
    </div>

</body>
</html>