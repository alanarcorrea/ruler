<div id="page-wrapper">

  <div class="row">
    <div class="col-lg-12">
      <h1 class="page-header">Alteração de Servidores</h1>
    </div>
  </div>

  <div class="row">
    <div class="col-lg-12">
      <div class="panel panel-default">
        <div class="panel-heading">
          Formulário de Alteração de Servidor
        </div>
        <div class="panel-body">
          <div class="row">
            <div class="col-lg-12">
              <form role="form" method="post" action="<?= base_url('servidores/grava_alteracao') ?>" enctype="multipart/form-data">

              <input type="hidden" name="id" value="<?= $servidor->id ?>">

                <div class="form-group">
                  <label>Hostname</label>
                  <input class="form-control" name="hostname" id="hostname" value="<?= $servidor->hostname ?>">

                </div>
                <div class="form-group">
                  <label>IP</label>
                  <input class="form-control" name="ip" id="ip" value="<?= $servidor->ip ?>">
                </div>
                <div class="form-group">
                  <label>Domínio</label>
                  <input class="form-control" name="dominio" id="dominio" value="<?= $servidor->dominio ?>">
                </div>
                <div class="form-group">
                  <label>DC</label>
                  <input class="form-control" name="dc" id="dc" value="<?= $servidor->dc ?>">
                </div>
                <div class="form-group">
                  <label for="sistemaOperacional">Sistema Operacional</label>
                  <select name="idSistemaOperacional" id="idSistemaOperacional" class="form-control" required>
                    <?php foreach ($sistemaOperacional as $so) { ?>
                      <option value="<?= $so->id ?>" <?= $so->id == $servidor->idSistemaOperacional ? 'selected' : '' ?>>
                        <?= $so->nome ?>
                      </option>
                      <?php } ?>
                  </select>
                </div>
                <br>
                <br>
                <div class="form-group">
                  <label>Atenção: Para alterar usuário e senha de acesso ao servidor, acesse a aba Credenciais! </label>
                </div>
                <button type="submit" class="btn btn-default">Enviar</button>
                <button type="reset" class="btn btn-default">Limpar</button>
              </form>
            </div>
            <!-- /.col-lg-6 (nested) -->
          </div>
          <!-- /.row (nested) -->
        </div>
        <!-- /.panel-body -->
      </div>
      <!-- /.panel -->
    </div>
    <!-- /.col-lg-12 -->
  </div>

  </body>

  </html>