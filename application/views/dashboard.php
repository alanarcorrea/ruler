<div id="page-wrapper">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">Dashboard</h1>
        </div>
    </div>

    <?php
        if ($this->session->has_userdata('mensa')) {
        $mensa = $this->session->flashdata('mensa');
        $tipo = $this->session->flashdata('tipo');

            if ($tipo) {
                echo "<div class='alert alert-success'>";
                echo "<strong>Successo!! </strong>" . $mensa; 
                echo "</div>";
            } else {
                echo "<div class='alert alert-danger'>";
                echo "<strong>Erro... </strong>" . $mensa; 
                echo "</div>";
            }                
        }            
    ?>

<div class="row">
                <div class="col-lg-3 col-md-6">
                    <div class="panel panel-primary">
                        <div class="panel-heading">
                            <div class="row">
                                <div class="col-xs-3">
                                    <i class="fa fa-comments fa-5x"></i>
                                </div>
                                <div class="col-xs-9 text-right">
                                    <div class="huge"><?= $wiki ?></div>
                                    <div>Wikis !</div>
                                </div>
                            </div>
                        </div>
                        <a href="<?= base_url('wiki') ?>">
                            <div class="panel-footer">
                                <span class="pull-left">Visualizar</span>
                                <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                                <div class="clearfix"></div>
                            </div>
                        </a>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6">
                    <div class="panel panel-green">
                        <div class="panel-heading">
                            <div class="row">
                                <div class="col-xs-3">
                                    <i class="fa fa-server fa-5x"></i>
                                </div>
                                <div class="col-xs-9 text-right">
                                    <div class="huge"><?= $servidores ?></div>
                                    <div>Servidores!</div>
                                </div>
                            </div>
                        </div>
                        <a href="<?= base_url('servidores') ?>">
                            <div class="panel-footer">
                                <span class="pull-left">Visualizar</span>
                                <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                                <div class="clearfix"></div>
                            </div>
                        </a>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6">
                    <div class="panel panel-yellow">
                        <div class="panel-heading">
                            <div class="row">
                                <div class="col-xs-3">
                                    <i class="fa fa-calendar-check-o fa-5x"></i>
                                </div>
                                <div class="col-xs-9 text-right">
                                    <div class="huge"><?= $equipamentos ?></div>
                                    <div>Equipamentos!</div>
                                </div>
                            </div>
                        </div>
                        <a href="<?= base_url('equipamentos') ?>">
                            <div class="panel-footer">
                                <span class="pull-left">Visualizar</span>
                                <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                                <div class="clearfix"></div>
                            </div>
                        </a>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6">
                    <div class="panel panel-red">
                        <div class="panel-heading">
                            <div class="row">
                                <div class="col-xs-3">
                                    <i class="fa fa-support fa-5x"></i>
                                </div>
                                <div class="col-xs-9 text-right">
                                    <div class="huge">0</div>
                                    <div>Alertas de Garantia!</div>
                                </div>
                            </div>
                        </div>
                        <a href="#">
                            <div class="panel-footer">
                                <span class="pull-left">Visualizar</span>
                                <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                                <div class="clearfix"></div>
                            </div>
                        </a>
                    </div>
                </div>
                <div class="row">
                <div class="col-lg-6">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            Notificações e Alertas
                        </div>
                        <!-- /.panel-heading -->
                        <div class="panel-body">
                            <?php if((base_url('conexao/conectar'))) { ?>
                                <div class="alert alert-success">
                                     <a href="#" class="alert-link">Servidor SRV01-ADS Online!</a>.
                                </div>
                           <?php } else{ ?>
                            <div class="alert alert-danger">
                                    <a href="#" class="alert-link">Servidor SRV01-ADS Offline! </a>.
                            </div>
                           <?php } ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        </body>
    </html>
</div>
