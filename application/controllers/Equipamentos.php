<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Equipamentos extends CI_Controller {

    public function __construct() {
        parent::__construct();
        //if(!$this->session->logado){
          //  redirect('home/login');
        //}
        $this->load->model('Equipamentos_model', 'equipamentos');
        $this->load->model('Credenciais_model', 'credenciais');
        $this->load->model('Fornecedores_model', 'fornecedores');

    }

    public function index() {

        $dados['equipamentos'] = $this->equipamentos->select();
        $this->load->view('include/side-menu');
        $this->load->view('equipamentos_view', $dados);
    }

 public function cadastrar() {
        $dados['equipamentos'] = $this->equipamentos->select();
        $dados['fornecedores'] = $this->fornecedores->select();
        $this->load->view('include/side-menu');
        $this->load->view('equipamentos_cadastro', $dados);
    }

public function grava_cadastro(){
        $dados['host'] = $this->input->post('nome');
        $dados['acesso'] = $this->input->post('acesso');
        $dados['usuario'] = $this->input->post('usuario');
        $dados['senha'] = $this->input->post('senha');
        $info['idCredenciais'] =$this->credenciais->insert($dados);
        if($info['idCredenciais'] != 0){
            $info['nome'] = $this->input->post('nome');
            $info['descricao'] = $this->input->post('descricao');
            $info['marca'] = $this->input->post('marca');
            $info['modelo'] = $this->input->post('modelo');
            $info['numeroSerie'] = $this->input->post('numeroSerie');
            $info['dataCompra'] = $this->input->post('dataCompra');
            $info['tipoGarantia'] = $this->input->post('tipoGarantia');
            $info['tempoGarantia'] = $this->input->post('tempoGarantia');
            $info['notaFiscal'] = $this->input->post('notaFiscal');
            $info['idFornecedor'] = $this->input->post('idFornecedor');
            if($this->equipamentos->insert($info)){
                $mensa = "Equipamento e Credenciais cadastradas!";
                $tipo = 1;
            } else {
                $mensa = "Equipamento não cadastrado!";
                $tipo = 0;
            }
        }else{
            $mensa = "Credenciais não cadastradas";
            $tipo = 0;
        }   
    // atribui para variáveis de sessão "flash"
    $this->session->set_flashdata('mensa', $mensa);
    $this->session->set_flashdata('tipo', $tipo);
    // recarrega a view (index)
    redirect('equipamentos');    
    }    
    
}
