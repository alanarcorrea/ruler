<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Servidores extends CI_Controller {

    public function __construct() {
        parent::__construct();
        //if(!$this->session->logado){
          //  redirect('home/login');
        //}
        $this->load->model('Servidores_model', 'servidores');
        $this->load->model('Credenciais_model', 'credenciais');
        $this->load->model('SistemasOperacionais_model', 'sistemasOperacionais');
    }

    public function index() {
        $dados['servidor'] = $this->servidores->select();
        $this->load->view('include/side-menu');
        $this->load->view('servidores_view', $dados);
    }

    public function cadastrar() {
        $dados['servidor'] = $this->servidores->select();
        $dados['sistemaOperacional'] = $this->sistemasOperacionais->select();
        $this->load->view('include/side-menu');
        $this->load->view('servidores_cadastro', $dados);
    }

    public function grava_cadastro() {
        // recebe os dados do formulário
        $dados['hostname'] = $this->input->post('hostname');
        $dados['ip'] = $this->input->post('ip');
        $dados['dominio'] = $this->input->post('dominio');
        $dados['dc'] = $this->input->post('dc');
        $dados['idSistemaOperacional'] = $this->input->post('idSistemaOperacional');
        $info['idServidor'] = $this->servidores->insert($dados);
        if($info['idServidor'] != 0){
            $info['usuario'] = $this->input->post('usuario');
            $info['senha'] = $this->input->post('senha');
            $info['acesso'] = $dados['ip'];
            $info['host'] = $dados['hostname'];
            if($this->credenciais->insert($info)){
                $mensa = "Servidor e Credenciais cadastradas!";
                $tipo = 1;
            } else {
                $mensa = "Credenciais não cadastradas!";
                $tipo = 0;
            }
        }else{
            $mensa = "Servidor não cadastrado!";
            $tipo = 0;
        }    
        // atribui para variáveis de sessão "flash"
        $this->session->set_flashdata('mensa', $mensa);
        $this->session->set_flashdata('tipo', $tipo);

        // recarrega a view (index)
        redirect('servidores');
    }
    
    public function alterar($id) {
        $dados['servidor'] = $this->servidores->find($id);
        $dados['sistemaOperacional'] = $this->sistemasOperacionais->select();
        $this->load->view('include/side-menu');
        $this->load->view('servidores_alterar', $dados);
    }

    public function grava_alteracao() {
        $dados = $this->input->post();
        $mensa = "";
        if ($this->servidores->update($dados)) {
            $mensa .="Dados do servidor alterados corretamente!";
            $tipo = 1;
        } else {
            $mensa .= "Dados do servdor não foram alterados!";
            $tipo = 0;
        }

        $this->session->set_flashdata('mensa', $mensa);
        $this->session->set_flashdata('tipo', $tipo);

        redirect('servidores');
    }

       public function excluir($id) {
        $this->servidores->find($id);

        if ($this->servidores->delete($id)) {
            $mensa .= "Servidor excluído corretamente!";
            $tipo = 1;
        } else {
            $mensa .= "Não foi possível excluir o servidor!";
            $tipo = 0;
        }

        // atribui para variáveis de sessão "flash"
        $this->session->set_flashdata('mensa', $mensa);
        $this->session->set_flashdata('tipo', $tipo);

        // recarrega a view (index)
        redirect('servidores');
    }
    
}
