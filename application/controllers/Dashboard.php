<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends CI_Controller {
    public function __construct() {
        parent::__construct();
        $this->load->model('Wiki_model', 'wiki');
        $this->load->model('Servidores_model', 'servidores');
        $this->load->model('Equipamentos_model', 'equipamentos');
    }
  

    public function index() {
        $dados['wiki'] = $this->wiki->select_count();
        $dados['servidores'] = $this->servidores->select_count();
        $dados['equipamentos'] = $this->equipamentos->select_count();
        $this->load->view('include/side-menu');
        $this->load->view('dashboard', $dados);
    }
}