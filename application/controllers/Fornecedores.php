<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Fornecedores extends CI_Controller {
    
    public function __construct() {
        parent::__construct();
         //if(!$this->session->logado){
        //redirect('home/login');
    //}
        $this->load->model('Fornecedores_model', 'fornecedores');
    }
    
    public function index() {
        
        $dados['fornecedores'] = $this->fornecedores->select();
        $this->load->view('include/side-menu');
        $this->load->view('fornecedores_view', $dados);
    }
    
    public function cadastrar() {
        $dados['fornecedores'] = $this->fornecedores->select();
        $this->load->view('include/side-menu');
        $this->load->view('fornecedores_cadastro', $dados);
    }

    public function grava_cadastro() {
        $dados = $this->input->post();
        $mensa = "";
        if ($this->fornecedores->insert($dados)) {
            $mensa .="Dados do fornecedor cadastrados corretamente!";
            $tipo = 1;
        } else {
            $mensa .= "Dados do fornecedor não foram cadastrados!";
            $tipo = 0;
        }

        $this->session->set_flashdata('mensa', $mensa);
        $this->session->set_flashdata('tipo', $tipo);

        redirect('fornecedores');
    }

     public function alterar($id) {
        $dados['fornecedor'] = $this->fornecedores->find($id);
        $this->load->view('include/side-menu');
        $this->load->view('fornecedores_alterar', $dados);
    }

    public function grava_alteracao() {
        $dados = $this->input->post();
        $mensa = "";
        if ($this->fornecedores->update($dados)) {
            $mensa .="Dados do fornecedor alterados corretamente!";
            $tipo = 1;
        } else {
            $mensa .= "Dados do fornecedor não foram alterados!";
            $tipo = 0;
        }

        $this->session->set_flashdata('mensa', $mensa);
        $this->session->set_flashdata('tipo', $tipo);

        redirect('fornecedores');
    }
    
    public function excluir($id) {
        $this->fornecedores->find($id);

        if ($this->fornecedores->delete($id)) {
            $mensa .= "Fornecedor excluído corretamente!";
            $tipo = 1;
        } else {
            $mensa .= "Não foi possível excluir o fornecedor!";
            $tipo = 0;
        }

        // atribui para variáveis de sessão "flash"
        $this->session->set_flashdata('mensa', $mensa);
        $this->session->set_flashdata('tipo', $tipo);

        // recarrega a view (index)
        redirect('fornecedores');
    }
}