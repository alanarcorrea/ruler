<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Wiki extends CI_Controller {

    public function __construct() {
        parent::__construct();
        //if(!$this->session->logado){
          //  redirect('home/login');
        //}
        $this->load->model('Wiki_model', 'wiki');
        $this->load->model('Categorias_model', 'categorias');
    }

    public function index() {

        $dados['wiki'] = $this->wiki->select();
        $dados['categoria'] = $this->categorias->select();
        $this->load->view('include/side-menu');
        $this->load->view('wiki_view', $dados);
    }

}
