<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $server ='LDAP://srv01-ads';    
        $this->ldap_server = $server;
        $this->conexao = ldap_connect($this->ldap_server);
        ldap_set_option($this->conexao, LDAP_OPT_PROTOCOL_VERSION, 3) or die('Impossível configurar a versão do LDAP!');
        ldap_set_option($this->conexao, LDAP_OPT_REFERRALS, 0);
        $this->base_dn = "ou=matriz,dc=ads,dc=local";
        $this->atributos = array('name');
    }

    public function index() {

        $this->verifica_sessao();
        $this->load->view('login');
    }

    public function verifica_sessao(){
        if(!($this->session->logado)){
            redirect('home/login');
        }
    }

    public function login() {
        $this->load->view('login');
    }

    public function logar(){
        $this->load->model('Login_model', 'loginM');
        $email = $this->input->post('email');
        $senha = $this->input->post('senha');
        $usuario = $this->loginM->verificaUsuario($email, $senha);
        if(isset($usuario)){
            redirect("dashboard");
        }
        else{
            redirect();
        }
    }






    public function logar2(){
    
        $user = $this->input->post('usuario');
        $password = $this->input->post('senha');
        $this->usuario = $user.'@ads.local';
        $this->senha = $password;
        if(ldap_bind($this->conexao, $this->usuario, $this->senha)){
            $sessao['usuario'] = $user;
            $sessao['logado'] = true;
            $sessao['senha'] = $password;
            //$teste = 'alana123';
            $this->session->set_userdata($sessao);
            $this->load->view('include/side-menu');
            $this->load->view('dashboard');
        }  
    //echo $this->session->usuario . "xxxx";
    else { $this->load->view('login');}
    //$this->load->view('dashboard');
        //  redirect ('dashboard');
        //}
        //redirect();
    }

    public function sair() {
            $this->session->sess_destroy();
            //$this->session->unset_userdata('usuario');
            redirect('home/login');
    }

    public function ok () {
    // using ldap bind
    $ldaprdn  = 'ads\administradr';     // ldap rdn or dn
    $ldappass = 'D3v@ads';  // associated password

    // connect to ldap server
    $ldapconn = ldap_connect("10.63.159.18")
        or die("Could not connect to LDAP server.");

    if ($ldapconn) {

        // binding to ldap server
        $ldapbind = ldap_bind($ldapconn, $ldaprdn, $ldappass);

        // verify binding
        if ($ldapbind) {
            echo "LDAP bind successful...";
            $filter = "(uid=alana)";
                $result = ldap_search($ldapconn, "ou=matriz,dc=ads,dc=local", $filter) or exit("Unable to search");
                $entries = ldap_get_entries($ldapconn, $result);
                    print "<pre>";
                    print_r($entries);
                    print "</pre>";
        }else {
            echo "LDAP bind failed...".$ldaprdn;
            echo "OI";
        }

    }


    //        $ldap_dn = "ou=matriz,dc=ads,dc=local";
    //        $ldap_username = "ads'\'administrador";
    //        $ldap_password = "D3v@ads";
    //
    //        $ldap_con = ldap_connect("10.63.159.18");
    //
    //        ldap_set_option($ldap_con, LDAP_OPT_PROTOCOL_VERSION, 3);
    //
    //        if (ldap_bind($ldap_con, $ldap_dn, $ldap_password)) {
    //
    //            echo "Bind successful!";

    //            $filter = "(uid=newton)";
    //            $result = ldap_search($ldap_con, "dc=example,dc=com", $filter) or exit("Unable to search");
    //            $entries = ldap_get_entries($ldap_con, $result);
    //            //$entries->objectClass;
    //            foreach ($entries as $entrie) {
    //                print "<pre>";
    //                print_r($entrie['mail'][0]);
    //                print "</pre>";
    //            }
            else {
                echo "Invalid user/pass or other errors!";
            }
        }

    }
