<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Credenciais extends CI_Controller {

    public function __construct() {
        parent::__construct();
        //if(!$this->session->logado){
           // redirect('home/login');
        //}
        $this->load->model('Credenciais_model', 'credenciais');
    }

    public function index() {

        $dados['credenciais'] = $this->credenciais->select();
        $this->load->view('include/side-menu');
        $this->load->view('credenciais_view', $dados);
    }
    

}
