<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Usuarios extends CI_Controller {
    private $ldap_server;
    private $conexao;
    private $usuario;
    public $usuario_ad;
    private $senha;
    private $base_dn;
    private $filtro;
    private $atributos;
    
    
    public function __construct() {
        parent::__construct();
        $this->ldap_server = 'LDAP://srv01-ads';
        $this->conexao = ldap_connect($this->ldap_server);
        ldap_set_option($this->conexao, LDAP_OPT_PROTOCOL_VERSION, 3) or die('Impossível configurar a versão do LDAP!');
        ldap_set_option($this->conexao, LDAP_OPT_REFERRALS, 0);
        $this->base_dn = "ou=matriz,dc=ads,dc=local";
        $this->atributos = array('name');
    }
    
    public function index() {
        $this->autentica('administrador','Admin123');
        $dados['entries'] = $this->busca_nome();
        $this->load->view('include/side-menu');
        $this->load->view('usuarios_view', $dados);
    }

    public function cadastrar(){
        $this->load->view('include/side-menu');
        $this->load->view('usuarios_cadastro');
    }

   public function grava_cadastro() {
        if($this->conexao){
            if(ldap_bind($this->conexao, 'ads\administrador', 'Admin123')){
                $dados["name"] = $this->input->post('name');
                $name = $dados["name"];
                $dn = 'CN='.$name.',OU=matriz,DC=ads,DC=local';
                $dados["givenname"] = $this->input->post('givenname');
                $dados["userprincipalname"] = $this->input->post('userprincipalname');
                $dados["sn"] = $this->input->post('sn');
                $dados["objectclass"] = "user";
                $newPassword = $this->input->post('password');
                $encoded_newPassword = "{MD5}" . base64_encode( pack( "H*", sha1( $newPassword ) ) );
                $dados["userPassword"] = "$encoded_newPassword";

                if (ldap_add($this->conexao, $dn, $dados)){
                    $mensa .= "Usuário cadastrado corretamente";
                    $tipo = 1;      
                }else{
                    $mensa .= "Erro ao cadastrar usuário";
                    $tipo = 0;
                }
                $mensa .= "Erro ao conectar bind";
                $tipo = 0;
            }
            $mensa .= "Erro ao conectar ao servidor";
            $tipo = 0;   
        }
        $this->session->set_flashdata('mensa', $mensa);
        $this->session->set_flashdata('tipo', $tipo);
        redirect(base_url('usuarios'));
    }
 

    
    public function busca_nome(){
        $this->filtro = "(objectCategory=person)";
        $ldap_result = ldap_search($this->conexao, $this->base_dn, $this->filtro, $this->atributos);
        $entries = ldap_get_entries($this->conexao, $ldap_result);
        return $entries;
    }
    
    public function autentica($usuario, $senha){
        $this->usuario = $usuario.'@ads.local';
        $this->senha = $senha;
        if(ldap_bind($this->conexao, $this->usuario, $this->senha)){
            $this->usuario_ad = $usuario;
            return true;
        }
        else
            return false;
    }
        
}
