<?php 

class Equipamentos_Model extends CI_Model {
    
    public function select() {        
        $sql = "SELECT * FROM equipamentos e ";
        $sql .= "WHERE e.ativo=1 ";
        $sql .= "ORDER BY e.nome";
        $query = $this->db->query($sql);
        return $query->result();
    }

    public function select_count() {        
        $sql = "select count(id) as equipamentos from equipamentos";
        $query = $this->db->query($sql);
        return $query->row()->equipamentos;
    }
    
    
    public function ativar($id){
        $sql = "UPDATE equipamentos SET ativo=1 WHERE id=$id";
        $this->db->query($sql); 
    }
    
    public function insert($equipamento) {
        $this->db->insert('equipamentos', $equipamento);
        $id = $this->db->insert_id();
        return $id;
    }
    
    public function update($equipamento) {  
       $this->db->where('id', $equipamento['id']);
       return $this->db->update('equipamentos', $equipamento);   
    }
    
    public function delete($id) {
        $this->db->where('id', $id);
        return $this->db->delete('equipamentos'); 
    }

    public function find($id) {        
        $sql = "SELECT * FROM equipamentos WHERE id = $id";
        $query = $this->db->query($sql);
        return $query->row();        
    }

}