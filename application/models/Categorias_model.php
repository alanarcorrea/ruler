<?php 

class Categorias_Model extends CI_Model {
    
    public function select() {        
        $sql = "SELECT * FROM categorias c ";
        $sql .= "ORDER BY c.nome";
        $query = $this->db->query($sql);
        return $query->result();
    }
    
}