<?php 

class Wiki_Model extends CI_Model {
    
    public function select() {        
        $sql = "SELECT * FROM wiki w ";
        $sql .= "WHERE w.ativo=1 ";
        $sql .= "ORDER BY w.titulo";
        $query = $this->db->query($sql);
        return $query->result();
    }

    public function select_count() {        
         $sql = "select count(id) as wiki from wiki";
        $query = $this->db->query($sql);
        return $query->row()->wiki;
    }
   
    
    public function insert($wiki) {
         return $this->db->insert('wiki', $wiki);
    }
    
    public function update($wiki) {  
       $this->db->where('id', $wiki['id']);
       return $this->db->update('wiki', $wiki);   
    }
    
    public function delete($id) {
        $this->db->where('id', $id);
        return $this->db->delete('wiki'); 
    }

    public function find($id) {        
        $sql = "SELECT * FROM wiki WHERE id = $id";
        $query = $this->db->query($sql);
        return $query->row();        
    }

}