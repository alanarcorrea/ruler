<?php 

class SistemasOperacionais_Model extends CI_Model {
    
    public function select() {        
        $sql = "SELECT * FROM sistemasoperacionais s ";
        $sql .= "ORDER BY s.nome";
        $query = $this->db->query($sql);
        return $query->result();
    }

    public function find($id) {        
        $sql = "SELECT * FROM sistemasoperacionais WHERE id = $id";
        $query = $this->db->query($sql);
        return $query->row();        
    }

}