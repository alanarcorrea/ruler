<?php 

class Credenciais_Model extends CI_Model {
    
    public function select() {        
        $sql = "SELECT * FROM credenciais c ";
        $sql .= "WHERE c.ativo=1 ";
        $sql .= "ORDER BY c.host";
        $query = $this->db->query($sql);
        return $query->result();
    }

    public function select_desativados() {        
        $sql = "SELECT * FROM credenciais c ";
        $sql .= "WHERE c.ativo=0 ";
        $sql .= "ORDER BY c.host";
        $query = $this->db->query($sql);
        return $query->result();
    }
    
    public function ativar($id){
        $sql = "UPDATE credenciais SET ativo=1 WHERE id=$id";
        $this->db->query($sql); 
    }
    
     public function desativar($id){
        $sql = "UPDATE credenciais SET ativo=0 WHERE id=$id";
        $this->db->query($sql); 
    }
    
    public function insert($credencial) {
        $this->db->insert('credenciais', $credencial);
        $id = $this->db->insert_id();
        return $id;
    }
    
    public function update($credencial) {  
       $this->db->where('id', $credencial['id']);
       return $this->db->update('credenciais', $credencial);   
    }
    
    public function delete($id) {
        $this->db->where('id', $id);
        return $this->db->delete('credenciais'); 
    }

    public function find($id) {        
        $sql = "SELECT * FROM credenciais WHERE id = $id";
        $query = $this->db->query($sql);
        return $query->row();        
    }

}