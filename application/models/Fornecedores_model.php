<?php 

class Fornecedores_Model extends CI_Model {
    
    public function select() {        
        $sql = "SELECT * FROM fornecedores f ";
        $sql .= "WHERE f.ativo=1 ";
        $sql .= "ORDER BY f.razaoSocial";
        $query = $this->db->query($sql);
        return $query->result();
    }
    
    public function insert($fornecedor) {
         return $this->db->insert('fornecedores', $fornecedor);
    }
    
    public function update($fornecedor) {  
       $this->db->where('id', $fornecedor['id']);
       return $this->db->update('fornecedores', $fornecedor);   
    }
    
   public function delete($id) {
        $sql = "UPDATE fornecedores SET ativo=0 WHERE id = $id";
        $this->db->query($sql);
      
    }

    public function find($id) {        
        $sql = "SELECT * FROM fornecedores WHERE id = $id";
        $query = $this->db->query($sql);
        return $query->row();        
    }

}