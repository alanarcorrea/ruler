<?php

class Servidores_Model extends CI_Model {
    
    public function select() {
        $sql = "SELECT s.*, o.nome as sistemaOperacional FROM servidores s ";
        $sql .= "INNER JOIN sistemasoperacionais o ";
        $sql .= "WHERE s.idSistemaOperacional=o.id AND ativo=1 ";
        $sql .= "ORDER BY s.hostname";
        $query = $this->db->query($sql);
        return $query->result();
    }
    
    public function select_desativados() {
        $sql = "SELECT * FROM servidores s ";
        $sql .= "WHERE s.ativo=0 ";
        $sql .= "ORDER BY s.hostname";
        $query = $this->db->query($sql);
        return $query->result();
    }

    public function select_count() {        
        $sql = "select count(id) as servidores from servidores";
        $query = $this->db->query($sql);
        return $query->row()->servidores;
    }
    
    
    public function ativar($id){
        $sql = "UPDATE servidores SET ativo=1 WHERE id=$id";
        $this->db->query($sql);
    }
    
    public function desativar($id){
        $sql = "UPDATE servidores SET ativo=0 WHERE id=$id";
        $this->db->query($sql);
    }
    
    public function insert($servidor) {
        $this->db->insert('servidores', $servidor);
        $id = $this->db->insert_id();
        return $id;
    }
    
    public function update($servidor) {
        $this->db->where('id', $servidor['id']);
        return $this->db->update('servidores', $servidor);
    }
    
    public function delete($id) {
        $sql = "UPDATE servidores SET ativo=0 WHERE id = $id";
        $this->db->query($sql);
      
    }
    
    public function find($id) {
        $sql = "SELECT * FROM servidores WHERE id = $id";
        $query = $this->db->query($sql);
        return $query->row();
    }
    
}