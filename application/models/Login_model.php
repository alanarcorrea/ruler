<?php 

class Login_Model extends CI_Model {
    
    public function select() {        
        $sql = "SELECT * FROM wiki w ";
        $sql .= "WHERE w.ativo=1 ";
        $sql .= "ORDER BY w.titulo";
        $query = $this->db->query($sql);
        return $query->result();
    }
    
    public function insert($wiki) {
         return $this->db->insert('wiki', $wiki);
    }
    
    public function update($wiki) {  
       $this->db->where('id', $wiki['id']);
       return $this->db->update('wiki', $wiki);   
    }
    
    public function delete($id) {
        $this->db->where('id', $id);
        return $this->db->delete('wiki'); 
    }

    public function find($id) {        
        $sql = "SELECT * FROM wiki WHERE id = $id";
        $query = $this->db->query($sql);
        return $query->row();        
    }

     public function verificaUsuario($email, $senha) {
        $sql = "SELECT * FROM login WHERE email = ? AND senha = ?";
        $query = $this->db->query($sql, array($email, md5($senha)));
        return $query->row();
    }

}