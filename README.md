<img src="/doc/logo_ruler.png">

# Ruler : Administrar é tudo! 

## Objetivos
Ruler é um sistema desenvolvido com foco no administrador de redes e servidores de uma empresa ou ambiente técnico. 
Ele visa disponibilizar ferramentas para facilitar a administração do dia-a-dia fornecendo integração com servidor de AD para controle de usuários. 
Além disso, conta com diversas funcionalidades que permitem a fácil visualização de informações e alertas significativos, poupando um tempo precioso para você se concentrar em tarefas ainda mais improtantes.
Tenha o controle da sua rede em suas mãos com o Ruler!

## Funcionalidades Gerais do Sistema

* Dashboard com informações básicas
* Teste de conexão com o servidor de Active Directory;
* Listagem de usuários do Active Directory;
* Criação de usuários do Active Directory;
* Cadastro e controle de servidores
* Cadastro e controle de Equipamentos
* Cadastro e controle de Fornecedores
* Cadastro e controle de Credenciais
* Controle de garantia de equipamentos
* Wiki
* Relatório de servidores


## Plataformas 

Função            | Plataforma    | Sistema Operacional
:-------:         | :--------:    | :-----------------:
Aplicação         | Física        | Windows 10
Banco de Dados    | Virtualizada  | Debian 7
Active Directory  | Virtualizada  | Windows Server 2012 R2


### Aplicação
Desenvolvida na linguagem PHP com uso do framework Codeigniter e Apache (via Xampp). 
Conporta todo o código de CRUDS e comunicação com o banco de dados (via Codeigniter) e com o Active Directory (via biblioteca DLAP do PHP).

* Tela de Login

<img src="/doc/ap01.png">
 
 * Tela da Dashboard

<img src="/doc/ap02.png">

* Tela de Usuários do AD

<img src="/doc/ap03.png">

* Tela de listagem de credenciais

<img src="/doc/ap04.png">

### Banco de Dados
O banco de dados Mysql utilizado para essa aplicação está instalado na plataforma virtualizada, junto também ao PhpMyAdmin para gerenciamento via browser do BD.

* Tela da VM de BD com Debian

<img src="/doc/bd01.png">

* Tela do PHPMyAdmin desta VM

<img src="/doc/bd02.png">

### Active Directory
O servidor de AD foi instalado de forma básica, somente com a função de AD configurada. Os usuários estão sendo sincronizados com a aplicação, assim como está recendo comandos para criação de novos usuaríos, ambos através da biblioteca de LDAP do PHP. 

* Tela de usuários de AD

<img src="/doc/ad01.png">

## Requisitos Funcionais

* Inserir/Editar/Excluir/Listar servidores
* Inserir/Editar/Excluir/Listar equipamentos
* Inserir/Editar/Excluir/Listar credenciais
* Inserir/Editar/Excluir/Listar fornecedores
* Inserir/Editar/Excluir/Listar wikis
* Visualizar informações pertinentes á rede e conexão ao servidor de AD na Dashboard
* Controlar a garantia dos equipamentos cadastrados
* Emitir relatórios de equipamentos/credenciais/servidores/usuários
* Listar usuários do servidor de AD
* Criar usuário de AD

## Requisitos Não Funcionais

* Uso de design responsivo
* Compatibilidade com SO Windows

## Diagrama ER

<img src="/doc/er.png">

## Modelagem do Sistema

<img src="/doc/modelagem.png">
