-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

-- -----------------------------------------------------
-- Schema ruler
-- -----------------------------------------------------

-- -----------------------------------------------------
-- Schema ruler
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `ruler` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci ;
USE `ruler` ;

-- -----------------------------------------------------
-- Table `ruler`.`Usuarios`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `ruler`.`Usuarios` (
  `id` INT NOT NULL AUTO_INCREMENT COMMENT '',
  `nome` VARCHAR(45) NOT NULL COMMENT '',
  `email` VARCHAR(45) NOT NULL COMMENT '',
  `senha` VARCHAR(45) NOT NULL COMMENT '',
  PRIMARY KEY (`id`)  COMMENT '')
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `ruler`.`SistemasOperacionais`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `ruler`.`SistemasOperacionais` (
  `id` INT NOT NULL AUTO_INCREMENT COMMENT '',
  `nome` VARCHAR(100) NOT NULL COMMENT '',
  PRIMARY KEY (`id`)  COMMENT '')
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `ruler`.`Credenciais`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `ruler`.`Credenciais` (
  `id` INT NOT NULL AUTO_INCREMENT COMMENT '',
  `host` VARCHAR(60) NOT NULL COMMENT '',
  `acesso` VARCHAR(100) NOT NULL COMMENT '',
  `usuario` VARCHAR(45) NOT NULL COMMENT '',
  `senha` VARCHAR(45) NOT NULL COMMENT '',
  `descricao` VARCHAR(150) NULL COMMENT '',
  `ativo` INT(1) NOT NULL DEFAULT 1 COMMENT '',
  PRIMARY KEY (`id`)  COMMENT '')
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `ruler`.`Servidores`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `ruler`.`Servidores` (
  `id` INT NOT NULL AUTO_INCREMENT COMMENT '',
  `hostname` VARCHAR(45) NOT NULL COMMENT '',
  `ip` VARCHAR(20) NOT NULL COMMENT '',
  `dominio` VARCHAR(60) NOT NULL COMMENT '',
  `dc` VARCHAR(300) NULL COMMENT '',
  `ativo` INT(1) NOT NULL DEFAULT 1 COMMENT '',
  `idSistemaOperacional` INT NOT NULL COMMENT '',
  `idCredenciais` INT NOT NULL COMMENT '',
  PRIMARY KEY (`id`)  COMMENT '',
  INDEX `fk_Servidores_SistemasOperacionais1_idx` (`idSistemaOperacional` ASC)  COMMENT '',
  INDEX `fk_Servidores_Credenciais1_idx` (`idCredenciais` ASC)  COMMENT '',
  CONSTRAINT `fk_Servidores_SistemasOperacionais1`
    FOREIGN KEY (`idSistemaOperacional`)
    REFERENCES `ruler`.`SistemasOperacionais` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_Servidores_Credenciais1`
    FOREIGN KEY (`idCredenciais`)
    REFERENCES `ruler`.`Credenciais` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `ruler`.`Fornecedores`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `ruler`.`Fornecedores` (
  `id` INT NOT NULL AUTO_INCREMENT COMMENT '',
  `razaoSocial` VARCHAR(150) NOT NULL COMMENT '',
  `endereco` VARCHAR(200) NOT NULL COMMENT '',
  `email` VARCHAR(45) NOT NULL COMMENT '',
  `telefone` VARCHAR(45) NOT NULL COMMENT '',
  `bairro` VARCHAR(60) NOT NULL COMMENT '',
  `cidade` VARCHAR(60) NOT NULL COMMENT '',
  `estado` VARCHAR(60) NOT NULL COMMENT '',
  `cep` VARCHAR(10) NOT NULL COMMENT '',
  `cnpj` VARCHAR(30) NOT NULL COMMENT '',
  `ativo` INT(1) NOT NULL DEFAULT 1 COMMENT '',
  `site` VARCHAR(200) NULL COMMENT '',
  PRIMARY KEY (`id`)  COMMENT '')
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `ruler`.`Login`
-- ----------------------------------s-------------------
CREATE TABLE IF NOT EXISTS login (
  `id` INT NOT NULL AUTO_INCREMENT ,
  `email` VARCHAR(50) NOT NULL ,
  `senha` VARCHAR(32) NOT NULL,
  PRIMARY KEY (`id`)  )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `ruler`.`Equipamentos`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `ruler`.`Equipamentos` (
  `id` INT NOT NULL AUTO_INCREMENT COMMENT '',
  `nome` VARCHAR(150) NOT NULL COMMENT '',
  `descricao` VARCHAR(250) NOT NULL COMMENT '',
  `marca` VARCHAR(45) NOT NULL COMMENT '',
  `modelo` VARCHAR(60) NOT NULL COMMENT '',
  `numeroSerie` VARCHAR(45) NOT NULL COMMENT '',
  `dataCompra` DATE NOT NULL COMMENT '',
  `tipoGarantia` VARCHAR(45) NOT NULL COMMENT '',
  `tempoGarantia` INT(3) NOT NULL COMMENT '',
  `notaFiscal` VARCHAR(260) NULL COMMENT '',
  `ativo` INT(1) NOT NULL DEFAULT 1 COMMENT '',
  `idFornecedor` INT NOT NULL COMMENT '',
  `idCredenciais` INT NOT NULL COMMENT '',
  PRIMARY KEY (`id`)  COMMENT '',
  INDEX `fk_Equipamentos_Fornecedor1_idx` (`idFornecedor` ASC)  COMMENT '',
  INDEX `fk_Equipamentos_Credenciais1_idx` (`idCredenciais` ASC)  COMMENT '',
  CONSTRAINT `fk_Equipamentos_Fornecedor1`
    FOREIGN KEY (`idFornecedor`)
    REFERENCES `ruler`.`Fornecedores` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_Equipamentos_Credenciais1`
    FOREIGN KEY (`idCredenciais`)
    REFERENCES `ruler`.`Credenciais` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `ruler`.`Categorias`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `ruler`.`Categorias` (
  `id` INT NOT NULL AUTO_INCREMENT COMMENT '',
  `nome` VARCHAR(60) NOT NULL COMMENT '',
  PRIMARY KEY (`id`)  COMMENT '')
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `ruler`.`Wiki`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `ruler`.`Wiki` (
  `id` INT NOT NULL AUTO_INCREMENT COMMENT '',
  `titulo` VARCHAR(45) NOT NULL COMMENT '',
  `descricao` VARCHAR(600) NOT NULL COMMENT '',
  `anexo` VARCHAR(45) NULL COMMENT '',
  `ativo` INT(1) NOT NULL DEFAULT 1 COMMENT '',
  `idCategoria` INT NOT NULL COMMENT '',
  PRIMARY KEY (`id`)  COMMENT '',
  INDEX `fk_Wiki_Categorias_idx` (`idCategoria` ASC)  COMMENT '',
  CONSTRAINT `fk_Wiki_Categorias`
    FOREIGN KEY (`idCategoria`)
    REFERENCES `ruler`.`Categorias` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
